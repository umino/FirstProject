//
//  SecondTabDataManager.swift
//  MyAppViper
//
//  Created by thi la on 3/6/18.
//

class SecondTabDataManager: SecondTabDataManagerProtocol {
    weak var interactor: SecondTabInteractorProtocol?
    
    func retrievePostList() -> [PostModel] {
        if let postList = Helper.getPostListFromLocalFile() {
            return postList
        } else {
            return [PostModel]()
        }
    }
}
