//
//  SecondTabCollectionCell.swift
//  MyAppViper
//
//  Created by thi la on 3/6/18.
//

import UIKit

class SecondTabCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var ivThumbnail: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbContent: UILabel!
    @IBOutlet weak var childContainer: UIView!
    
    func set(forPost post: PostModel) {
        lbTitle.text = post.title
        lbContent.text = post.content?.components(separatedBy: ".")[0]
        if post.imageUrl != nil {
            ivThumbnail.sd_setImage(with: URL(string: post.imageUrl!))
        }
    }
}
