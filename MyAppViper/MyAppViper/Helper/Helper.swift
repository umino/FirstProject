//
//  Helper.swift
//  MyAppViper
//
//  Created by thi la on 3/6/18.
//

import Foundation
import UIKit

class Helper {
    static func getPostListFromLocalFile() -> [PostModel]? {
        var postList: [PostModel]?
        
        if let filePath = Bundle.main.path(forResource: "Data", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: filePath), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? [String: AnyObject]{
                    if let postArray = jsonResult["posts"] as? [[String: AnyObject]] {
                        postList = [PostModel]()
                        
                        for post in postArray {
                            let post = PostModel(dictionary: post)
                            postList?.append(post)
                        }
                        return postList!
                    }
                }
            } catch {
                // handle error
            }
        }
        return nil
    }
}
