//
//  PostDetailPresenter.swift
//  MyAppViper
//
//  Created by thi la on 3/5/18.
//

class PostDetailPresenter: PostDetailPresenterProtocol {
    weak var view: PostDetailViewProtocol?
    var wireFrame: PostDetailWireFrameProtocol?
    var post: PostModel?
    
    func viewDidLoad() {
        view?.showPostDetail(forPost: post!)
    }
}
