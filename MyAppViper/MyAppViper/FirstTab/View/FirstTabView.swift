//
//  FirstTabView.swift
//  MyAppViper
//
//  Created by thi la on 3/5/18.
//

import UIKit

class FirstTabView: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var presenter: FirstTabPresenterProtocol?
    var postList: [PostModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
}

extension FirstTabView: FirstTabProtocol {
    func showPosts(with posts: [PostModel]) {
        postList = posts
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.reloadData()
    }
}

extension FirstTabView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableCell", for: indexPath) as? FirstTabTableViewCell else {
            fatalError("The dequeued cell is not an instance of FirstTabTableViewCell.")
        }
        
        let post = postList[indexPath.row]
        cell.set(forPost: post)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postList.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.showPostDetail(forPost: postList[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        
    }
}
