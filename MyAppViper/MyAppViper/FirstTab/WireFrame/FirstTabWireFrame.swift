//
//  FirstTabWireFrame.swift
//  MyAppViper
//
//  Created by thi la on 3/6/18.
//

import UIKit

class FirstTabWireFrame: FirstTabWireFrameProtocol {
    
    static func createFirstTabModule() -> UIViewController {
        let navController = mainStoryboard.instantiateViewController(withIdentifier: "FirstToDetailNavController")
        if let view = navController.childViewControllers.first as? FirstTabView {
            let presenter: FirstTabPresenterProtocol = FirstTabPresenter()
            let interactor: FirstTabInteractorProtocol = FirstTabInteractor()
            let dataManager: FirstTabDataManagerProtocol = FirstTabDataManager()
            let wireFrame: FirstTabWireFrameProtocol = FirstTabWireFrame()
            
            
            view.presenter = presenter
            presenter.view = view
            presenter.wireFrame = wireFrame
            presenter.interactor = interactor
            interactor.presenter = presenter
            interactor.dataManager = dataManager
            dataManager.interactor = interactor
            
            return navController
        }
        return UIViewController()
    }
    
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    func presentPostDetailScreen(from view: FirstTabProtocol, forPost post: PostModel) {
        let postDetailViewController = PostDetailWireFrame.createPostDetailModule(forPost: post)
        
        if let sourceView = view as? UIViewController {
            sourceView.navigationController?.pushViewController(postDetailViewController, animated: true)
        }
    }
}
