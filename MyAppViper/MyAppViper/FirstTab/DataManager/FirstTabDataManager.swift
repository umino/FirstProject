//
//  FirstTabLocalDataManager.swift
//  MyAppViper
//
//  Created by thi la on 3/6/18.
//

class FirstTabDataManager: FirstTabDataManagerProtocol {
    weak var interactor: FirstTabInteractorProtocol?
    
    func retrievePostList() -> [PostModel] {
        if let postList = Helper.getPostListFromLocalFile() {
            return postList
        } else {
            return [PostModel]()
        }
    }
}
