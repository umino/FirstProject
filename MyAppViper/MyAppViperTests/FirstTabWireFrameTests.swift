//
//  FirstTabWireFrameTests.swift
//  MyAppViperTests
//
//  Created by thi la on 3/19/18.
//

import XCTest
import UIKit
@testable import MyAppViper

class FirstTabWireFrameTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testInitializeFirstTabView() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let navController = mainStoryboard.instantiateViewController(withIdentifier: "FirstToDetailNavController")
        let firstTabView = navController.childViewControllers.first as? FirstTabView
        
        XCTAssertNotNil(firstTabView, "Couldnot create FirstTabView")
    }
}
