//
//  AppDelegateTests.swift
//  MyAppViperTests
//
//  Created by thi la on 3/19/18.
//

import XCTest
import UIKit
@testable import MyAppViper

class AppDelegateTests: XCTestCase {
    
    var appDelegate: AppDelegate?
    var window: UIWindow?
    
    
    override func setUp() {
        super.setUp()
        window = UIWindow()
        appDelegate = AppDelegate()
        appDelegate?.window = window
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // after init
    func testInitializeScreen() {
        appDelegate?.rootViewController = RootWireFrame.createRootModule()
        XCTAssertNotNil(appDelegate?.rootViewController, "Could not create TabBar view controller")
    }
    
}
